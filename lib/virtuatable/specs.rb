# frozen_string_literal: true

module Virtuatable
  # This module holds declarations for shared examples and factories used
  # in service, in particular to test the standard behaviour of a route.
  # @author Vincent Courtois <courtois.vincent@outlook.com>
  module Specs
    autoload :Factories, 'virtuatable/specs/factories'
    autoload :Shared, 'virtuatable/specs/shared'
  end
end
