# frozen_string_literal: true

module Virtuatable
  module Helpers
    # This helper gives access to methods about user's session on the API.
    # @author Vincent Courtois <courtois.vincent@outlook.com>
    module Sessions
      # Checks the session of the user requesting the API and returns an error
      # if it either not exists with the given token, or the token is not given.
      #
      # @raise [Virtuatable::API::Errors::NotFound] if the session is not found
      #   or the token not given in the parameters of the request.
      # @raise [Virtuatable::API::Errors::BadRequest] if the session token is
      #   not correctly given in the parameters.
      #
      # @return [Arkaan::Authentication::Session] the current session of the user.
      def session
        return @session unless @session.nil?

        check_presence 'session_id'
        @session = session_model.find_by(session_id: params['session_id'])
        return api_not_found('session_id.unknown') if @session.nil?

        check_current_route_access @session
      end

      def check_current_route_access(tmp_session)
        tmp_session.account.groups.each do |group|
          routes_ids = group.routes.map(&:_id)
          return tmp_session if routes_ids.include?(current_route.id)
        end
        api_forbidden('session_id.forbidden')
      end

      def session_model
        Arkaan::Authentication::Session
      end
    end
  end
end
