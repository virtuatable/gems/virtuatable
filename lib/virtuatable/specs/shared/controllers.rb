# frozen_string_literal: true

module Virtuatable
  module Specs
    module Shared
      # This module loads the shared examples about a controller and/or a route.
      # These examples can be included to ensure the basic behaviours for a route
      # are implemented in each micro-service, without the hassle of rewriting it.
      #
      # @author Vincent Courtois <courtois.vincent@outlook.com>
      # rubocop:disable Metrics/ModuleLength
      module Controllers
        extend ActiveSupport::Concern

        @declared = false

        included do
          declare_loader :controller_specs, priority: 6
        end

        # This method is automatically called when loading the application with
        # the load_test! method in the builder.
        # rubocop:disable Metrics/AbcSize
        # rubocop:disable Metrics/MethodLength
        def load_controller_specs!
          # This avoids multiple re-declarations by setting a flag.
          return if self.class.class_variable_defined?(:@@controllers_declared)

          service = Virtuatable::Application.instance.builder.service

          # Shared examples for a standard route of the application.
          # These examples are configurable with a configuration hash given as
          # third parameters, available configuration keys are :
          # - :premium to know if the route is only accessible to premium apps
          # - :authenticated to know if the route needs authentication or not
          # rubocop:disable Metrics/BlockLength
          RSpec.shared_examples 'a route' do |verb, path|
            # These two variables are NOT declared in let! blocks so that we're able
            # to use them as traditional Ruby variable in if: options of describe blocks.
            service = Virtuatable::Application.instance.builder.service
            route = service.routes.find_by(verb: verb, path: path)

            let!(:verb) { route.verb }
            let!(:path) { route.path }

            # This user has no right to access anything because he has no group.
            let!(:account) { create(:random_account) }
            # A standard application to be able to make basic requests.
            let!(:application) { create(:random_premium_app, premium: route.premium) }

            # Tests written for each and every route
            describe 'Standard routes behaviours' do
              # Error scenario :
              # - The user makes a request on the API without giving an application key
              # - The API fails and returns an error code
              describe 'The Application key is not given' do
                before do
                  public_send verb, path
                end
                it 'Returns a 400 (Bad Request) status code' do
                  expect(last_response.status).to be 400
                end
                it 'Returns the correct body' do
                  expect(last_response.body).to include_json(
                    status: 400,
                    field: 'app_key',
                    error: 'required'
                  )
                end
              end

              # Error scenario :
              # - The user makes a request on the API and gives an unknown application key
              # - The API fails and returns an error code
              describe 'The application key is unknown' do
                before do
                  public_send verb, path, { app_key: BSON::ObjectId.new }
                end
                it 'Returns a 404 (Not Found) status code' do
                  expect(last_response.status).to be 404
                end
                it 'Returns the correct body' do
                  expect(last_response.body).to include_json(
                    status: 404,
                    field: 'app_key',
                    error: 'unknown'
                  )
                end
              end
            end

            describe 'Authenticated route behaviours', if: route.authenticated do
              let!(:session) { create(:random_session, account: account) }

              # Error scenario :
              # - The user request an authenticated route without providing a session ID
              # - The API fails and return an error code
              describe 'The session ID is not given' do
                before do
                  public_send verb, path, { app_key: application.app_key }
                end
                it 'Returns a 400 (Bad Request) status code' do
                  expect(last_response.status).to be 400
                end
                it 'Returns the correct body' do
                  expect(last_response.body).to include_json(
                    status: 400,
                    field: 'session_id',
                    error: 'required'
                  )
                end
              end

              # Error scenario :
              # - The user request an authenticated route with an unknown session ID
              # - The API fails and return an error code
              describe 'The session ID is unknown' do
                before do
                  public_send verb, path, {
                    app_key: application.app_key,
                    session_id: BSON::ObjectId.new
                  }
                end
                it 'Returns a 404 (Not Found) status code' do
                  expect(last_response.status).to be 404
                end
                it 'Returns the correct body' do
                  expect(last_response.body).to include_json(
                    status: 404,
                    field: 'session_id',
                    error: 'unknown'
                  )
                end
              end

              # Error scenario :
              # - The user request an authenticated route with a valid session ID
              # - The user has no right to access the route
              # - The API fails and return an error code
              describe 'The user has no right to access the resource' do
                before do
                  public_send verb, path, {
                    app_key: application.app_key,
                    session_id: session.session_id
                  }
                end
                it 'Returns a 403 (Forbidden) status code' do
                  expect(last_response.status).to be 403
                end
                it 'Returns the correct body' do
                  expect(last_response.body).to include_json(
                    status: 403,
                    field: 'session_id',
                    error: 'forbidden'
                  )
                end
              end
            end

            # Error scenario :
            # - A user tries to make a request on a premium route with an non-premium app
            # - The API fails and return an error code
            describe 'A non-premium application accesses a premium route', if: route.premium do
              let!(:forbidden_app) { create(:random_application) }

              before do
                public_send verb, path, {
                  app_key: forbidden_app.app_key
                }
              end
              it 'Returns a 403 (Forbidden) status code' do
                expect(last_response.status).to be 403
              end
              it 'Returns the correct body' do
                expect(last_response.body).to include_json(
                  status: 403,
                  field: 'app_key',
                  error: 'forbidden'
                )
              end
            end
          end
          # rubocop:enable Metrics/BlockLength

          # rubocop:disable Style/ClassVars
          @@controllers_declared = true
          # rubocop:enable Style/ClassVars
        end
        # rubocop:enable Metrics/MethodLength
        # rubocop:enable Metrics/AbcSize
      end
      # rubocop:enable Metrics/ModuleLength
    end
  end
end
