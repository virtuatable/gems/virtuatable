# frozen_string_literal: true

module Virtuatable
  module Specs
    # This module holds the shared examples used in services to automate tests.
    # @author Vincent Courtois <courtois.vincent@outlook.com
    module Shared
      autoload :Controllers, 'virtuatable/specs/shared/controllers'
    end
  end
end
