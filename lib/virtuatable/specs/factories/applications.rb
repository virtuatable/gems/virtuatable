# frozen_string_literal: true

module Virtuatable
  module Specs
    module Factories
      # Factories concerning applications used in shared examples
      # @author Vincent Courtois <courtois.vincent@outlook.com>
      module Applications
        extend ActiveSupport::Concern

        included do
          declare_loader :applications_factory, priority: 5
        end

        # rubocop:disable Metrics/MethodLength
        def load_applications_factory!
          # This avoids multiple re-declarations by setting a flag.
          return if self.class.class_variable_defined?(:@@apps_declared)

          FactoryBot.define do
            factory :vt_empty_application, class: Arkaan::OAuth::Application do
              # This factory creates an application with random value, useful
              # to make simple requests on any route as all routes require
              # the use of a valid application to be correctly called.
              factory :random_application do
                name { Faker::Alphanumeric.unique.alphanumeric(number: 16) }
                app_key { BSON::ObjectId.new }
                creator { association(:random_administrator) }
                premium { false }

                # This factory creates a premium application, mainly used for
                # registration and authentication purpose.
                factory :random_premium_app do
                  premium { true }
                end
              end
            end
          end

          # rubocop:disable Style/ClassVars
          @@apps_declared = true
          # rubocop:enable Style/ClassVars
        end
        # rubocop:enable Metrics/MethodLength
      end
    end
  end
end
