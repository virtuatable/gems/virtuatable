# frozen_string_literal: true

module Virtuatable
  module Specs
    module Factories
      # This module creates the factories concerning accounts, with or
      # without rights, and with random values for each field.
      # @author Vincent Courtois <courtois.vincent@outlook.com>
      module Sessions
        extend ActiveSupport::Concern

        included do
          declare_loader :sessions_factory, priority: 5
        end

        def load_sessions_factory!
          # This avoids multiple re-declarations by setting a flag.
          return if self.class.class_variable_defined?(:@@sessions_declared)

          FactoryBot.define do
            factory :vt_empty_session, class: Arkaan::Authentication::Session do
              # Creates a random account with no particular right. This
              # is useful to see when a user is NOT authorized to access
              # a resource. Add groups to access resources.
              factory :random_session do
                session_id { BSON::ObjectId.new }
              end
            end
          end

          # rubocop:disable Style/ClassVars
          @@sessions_declared = true
          # rubocop:enable Style/ClassVars
        end
      end
    end
  end
end
