# frozen_string_literal: true

module Virtuatable
  module Specs
    module Factories
      # This module creates the factories concerning accounts, with or
      # without rights, and with random values for each field.
      # @author Vincent Courtois <courtois.vincent@outlook.com>
      module Accounts
        extend ActiveSupport::Concern

        included do
          declare_loader :accounts_factory, priority: 5
        end

        # rubocop:disable Metrics/MethodLength
        def load_accounts_factory!
          # This avoids multiple re-declarations by setting a flag.
          return if self.class.class_variable_defined?(:@@accounts_declared)

          FactoryBot.define do
            factory :vt_empty_account, class: Arkaan::Account do
              # Creates a random account with no particular right. This
              # is useful to see when a user is NOT authorized to access
              # a resource. Add groups to access resources.
              factory :random_account do
                username do
                  Faker::Alphanumeric.unique.alphanumeric(
                    number: 16,
                    min_alpha: 16
                  )
                end
                password { 'super_secure_pwd' }
                password_confirmation { 'super_secure_pwd' }
                email { Faker::Internet.unique.safe_email }

                # This creates an administrator as "a user with access to
                # all the routes in a group". This is NOT a superuser.
                factory :random_administrator do
                  groups { [association(:random_admin_group)] }
                end
              end
            end
          end

          # rubocop:disable Style/ClassVars
          @@accounts_declared = true
          # rubocop:enable Style/ClassVars
        end
        # rubocop:enable Metrics/MethodLength
      end
    end
  end
end
