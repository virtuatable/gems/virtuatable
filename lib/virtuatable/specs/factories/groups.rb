# frozen_string_literal: true

module Virtuatable
  module Specs
    module Factories
      # Factories concerning groups used in shared examples
      # @author Vincent Courtois <courtois.vincent@outlook.com>
      module Groups
        extend ActiveSupport::Concern

        included do
          declare_loader :groups_factory, priority: 5
        end

        # rubocop:disable Metrics/MethodLength
        def load_groups_factory!
          # This avoids multiple re-declarations by setting a flag.
          return if self.class.class_variable_defined?(:@@groups_declared)

          FactoryBot.define do
            factory :vt_empty_group, class: Arkaan::Permissions::Group do
              # This creates a random group with access to zero routes. This group
              # won't give the users it's associated to ANY access rights.
              factory :random_group do
                slug do
                  Faker::Alphanumeric.alphanumeric(
                    number: 16,
                    min_alpha: 16
                  )
                end
                is_default { false }
                is_superuser { false }
                # We do NOT set the is_superuser flag to true as this factory is made to
                # test permissions when the route is in the list of accessible routes.
                factory :random_admin_group do
                  routes { Virtuatable::Application.instance.builder.service.routes.to_a }
                end
              end
            end
          end

          # rubocop:disable Style/ClassVars
          @@groups_declared = true
          # rubocop:enable Style/ClassVars
        end
        # rubocop:enable Metrics/MethodLength
      end
    end
  end
end
