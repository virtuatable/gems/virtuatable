# frozen_string_literal: true

module Virtuatable
  module Specs
    # This module holds standard factories you can use in services to
    # easily create new elements.
    # @author Vincent Courtois <courtois.vincent@outlook.com
    module Factories
      autoload :Accounts, 'virtuatable/specs/factories/accounts'
      autoload :Applications, 'virtuatable/specs/factories/applications'
      autoload :Groups, 'virtuatable/specs/factories/groups'
      autoload :Sessions, 'virtuatable/specs/factories/sessions'
    end
  end
end
