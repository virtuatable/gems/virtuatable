puts '> Création des hooks de nettoyage de la base de données'

RSpec.configure do |config|
  DatabaseCleaner.clean
  DatabaseCleaner[:mongoid].strategy = :truncation, {
    except: [
      'arkaan_monitoring_services',
      'arkaan_monitoring_routes'
    ]
  }
  config.after(:each) do
    DatabaseCleaner.clean
  end
end