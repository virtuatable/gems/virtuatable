module Controller
  # This controller is created to test the behaviours of the
  # Virtuatable::Controllers::Base class that is used in services.
  class Behaviours < Virtuatable::Controllers::Base

    # This route is here to test 404 errors raised by an exception
    api_route 'get', '/unknown/exception', options: { authenticated: false } do
      raise Virtuatable::API::Errors::NotFound.new(
        field: 'custom_field',
        error: 'unknown'
      )
    end

    # This route is here to test 404 errors raised by a method call
    api_route 'get', '/unknown/method', options: { authenticated: false } do
      api_not_found 'custom_field'
    end

    # This route is here to test 403 errors raised by an exception
    api_route 'get', '/forbidden/exception', options: { authenticated: false } do
      raise Virtuatable::API::Errors::Forbidden.new(
        field: 'custom_field',
        error: 'forbidden'
      )
    end

    # This route is here to test 403 errors raised by a method call
    api_route 'get', '/forbidden/method', options: { authenticated: false } do
      api_forbidden 'custom_field'
    end

    # This route is here to test 400 errors raised by an exception
    api_route 'get', '/bad_request/exception', options: { authenticated: false } do
      raise Virtuatable::API::Errors::BadRequest.new(
        field: 'custom_field',
        error: 'bad_request'
      )
    end

    # This route is here to test 400 errors raised by a method call
    api_route 'get', '/bad_request/method', options: { authenticated: false } do
      api_bad_request 'custom_field', message: 'bad_request'
    end

    # This route is here to test how uncaught errors are handled
    api_route 'get', '/error', options: { authenticated: false } do
      raise StandardError.new
    end

    # This route raises a Mongoid::Errors::Validation error by creating an account
    # with an empty username, so that the catch mecanism can be tested.
    api_route 'get', '/mongoid', options: { authenticated: false } do
      Arkaan::Account.new.save!
    end

    # This route tests the check_either_presence helper
    api_route 'get', '/either', options: { authenticated: false } do
      check_either_presence 'username', 'email', key: 'asked_fields'
      api_ok 'achieved'
    end

    # This route tests the check_presence helper for several fields
    api_route 'get', '/check', options: { authenticated: false } do
      check_presence 'username', 'email'
      api_ok 'achieved'
    end

    # This route exists to test everything related to authentication :
    # - the session ID is not given
    # - the session ID given is unkonw
    # - the user linked to the session has no rights to access the route
    api_route 'get', '/authentication' do
      api_ok 'achieved'
    end

    # This route exists to test the same nominal case about premium apps
    api_route 'get', '/premium', options: { premium: true } do
      api_ok 'achieved'
    end
  end
end