# These specifications are probably the most import ones.
# They describe the desired behaviours for the base controller
# class that is used in all services to declare routes correctly.
RSpec.describe Virtuatable::Controllers::Base do

  # This does reload the whole application, with steps :
  # - creation of the service and the instance.
  # - creation of the routes for the application.
  before do
    Virtuatable::Application.load_tests!('controllers', path: '../../..')
    all_routes = Arkaan::Monitoring::Route.all.to_a
    Virtuatable::Application.instance.builder.service.routes = all_routes

    # Declares the controller in RSpec to be tested as a Sinatra controller.
    def app; Controller::Behaviours.new; end
  end

  let!(:routes) { Virtuatable::Application.instance.builder.service.routes }
  let!(:group) {
    create(:group, slug: 'test_accounts_session', routes: routes)
  }
  let!(:babausse) { create(:babausse, groups: [group]) }
  let!(:application) { create(:application, creator: babausse) }
  let!(:params) { { app_key: application.app_key } }

  # This section generates tests for all the possible HTTP errors raised in the application.
  # It creates two test scenarios for each error type :
  # - the case where the error is raised with an exception
  # - the case where the error is raised by calling the helper method
  # Both case are created using notation conventions.
  describe 'HTTP errors with exceptions or method calls' do
    errors = [
      {code: 404, uri: 'unknown', label: 'Not Found'},
      {code: 403, uri: 'forbidden', label: 'Forbidden'},
      {code: 400, uri: 'bad_request', label: 'Bad Request'}
    ]
    errors.each do |error|
      describe "Triggered \"#{error[:label]}\" errors" do
        # For each type of error, we check the raise by exception, and by calling the
        # helper method, again it's done via naming conventions.
        ['exception', 'method']. each do |subroute|
          describe "Raised by an #{subroute}" do
            before do
              get "/#{error[:uri]}/#{subroute}", params
            end
            it 'Returns a 404 (Not Found) status code' do
              expect(last_response.status).to be error[:code]
            end
            it 'Returns the correct body' do
              expect(last_response.body).to include_json(
                status: error[:code],
                field: 'custom_field',
                error: error[:uri]
              )
            end
          end
        end
      end
    end
  end

  # This scenario handles a pretty common case :
  # - You correctly access the route you want to access
  # - Something in the route crashes and an uncaught error is raised.
  describe 'When an uncaught error is raised' do
    before do
      get '/error', params
    end
    it 'Returns a 500 (Internal server error) status code' do
      expect(last_response.status).to be 500
    end
    it 'Returns the correct body' do
      expect(last_response.body).to include_json(
        status: 500,
        field: 'unknown_field',
        error: 'StandardError'
      )
    end
  end

  # This scenario is another common case throughout our application :
  # - The user is trying to create or update a resource
  # - There is an error in the resource tha raises a validation error.
  describe 'When a Mongoid::Errors::Validations is raised' do
    before do
      get '/mongoid', params
    end
    it 'Returns a 500 (Internal server error) status code' do
      expect(last_response.status).to be 400
    end
    it 'Returns the correct body' do
      expect(last_response.body).to include_json(
        status: 400,
        field: 'username',
        error: 'required'
      )
    end
  end

  # This tests the #check_presence helper that requires all the
  # fields of the given list to be given as parameters.
  # Three cases are tested :
  # - NO field is given, and it should raise an error
  # - SOME fields are given but not all, again an error
  # - ALL fields are given, this time this shoudl be OK
  describe 'When all the fields of a list are required' do
    describe 'no field is given' do
      before do
        get '/check', params
      end
      it 'Returns a 400 (Bad Request) status code' do
        expect(last_response.status).to be 400
      end
      it 'Returns the correct body' do
        expect(last_response.body).to include_json(
          status: 400,
          field: 'username',
          error: 'required'
        )
      end
    end
    describe 'not all fields are given' do
      before do
        get '/check', params.merge(username: 'test')
      end
      it 'Returns a 400 (Bad Request) status code' do
        expect(last_response.status).to be 400
      end
      it 'Returns the correct body' do
        expect(last_response.body).to include_json(
          status: 400,
          field: 'email',
          error: 'required'
        )
      end
    end
    describe 'all fields are given' do
      before do
        get '/check', params.merge(username: 'test', email: 'test@mail.com')
      end
      it 'Returns a 200 (OK) status code' do
        expect(last_response.status).to be 200
      end
      it 'Returns the correct body' do
        expect(last_response.body).to include_json(
          message: 'achieved'
        )
      end
    end
  end

  # This tests the #check_either_presence helper that requires some of the
  # fields of the given list to be given as parameters. hree cases are tested :
  # - NO field is given, and it should raise an error
  # - SOME fields are given but not all, and it should be OK
  # - ALL fields are given, this time this shoudl be OK
  describe 'When either field of a list is required' do
    describe 'no field is given' do
      before do
        get '/either', params
      end
      it 'Returns a 400 (Bad Request) status code' do
        expect(last_response.status).to be 400
      end
      it 'Returns the correct body' do
        expect(last_response.body).to include_json(
          status: 400,
          field: 'asked_fields',
          error: 'required'
        )
      end
    end
    describe 'not all fields are given' do
      before do
        get '/either', params.merge(username: 'test')
      end
      it 'Returns a 200 (OK) status code' do
        expect(last_response.status).to be 200
      end
      it 'Returns the correct body' do
        expect(last_response.body).to include_json(
          message: 'achieved'
        )
      end
    end
    describe 'all fields are given' do
      before do
        get '/either', params.merge(username: 'test', email: 'test@mail.com')
      end
      it 'Returns a 200 (OK) status code' do
        expect(last_response.status).to be 200
      end
      it 'Returns the correct body' do
        expect(last_response.body).to include_json(
          message: 'achieved'
        )
      end
    end
  end

  it_should_behave_like 'a route', 'get', '/authentication'

  # This scenario tests the nominal scenario :
  # - the application key is given and valid
  # - the application is found and has rights to access the route
  # - the session ID is given and valid
  # - the user linked to the session has access to the route
  describe 'Nominal case, the user is allowed to access the resource' do
    let!(:session) { create(:session, account: babausse) }

    before do
      get '/authentication', params.merge(session_id: session.session_id)
    end
    it 'Returns a 200 (OK) status code' do
      expect(last_response.status).to be 200
    end
    it 'Returns the correct body' do
      expect(last_response.body).to include_json(message: 'achieved')
    end
  end

  it_should_behave_like 'a route', 'get', '/premium'

  describe 'Nominal case, the application is premium to access premium resource' do
    let!(:premium) { create(:premium_app, creator: babausse) }
    let!(:session) { create(:session, account: babausse) }

    before do
      get '/premium', { app_key: premium.app_key, session_id: session.session_id }
    end
    it 'Returns a 200 (OK) status code' do
      expect(last_response.status).to be 200
    end
    it 'Returns the correct body' do
      expect(last_response.body).to include_json(
        message: 'achieved'
      )
    end
  end
end