RSpec.describe do
  def app
    Controllers::Responses.new
  end

  describe :api_ok do
    before do
      get '/ok'
    end
    it 'Returns a 200 (OK) status code' do
      expect(last_response.status).to be 200
    end
    it 'Returns the correct body' do
      expect(last_response.body).to include_json(message: 'message')
    end
  end
  describe :api_list do
    describe 'default mapper' do
      before do
        get '/list'
      end
      it 'Returns a 200 (OK) status code' do
        expect(last_response.status).to be 200
      end
      it 'Returns the correct body' do
        expect(last_response.body).to include_json(
          count: 1,
          items: [{key: 'value'}]
        )
      end
    end
    describe 'custom mapper' do
      before do
        get '/other_list'
      end
      it 'Returns a 200 (OK) status code' do
        expect(last_response.status).to be 200
      end
      it 'Returns the correct body' do
        expect(last_response.body).to include_json(
          count: 1,
          items: [{try: 'another'}]
        )
      end
    end
  end
  describe :api_item do
    before do
      get '/item'
    end
    it 'Returns a 200 (OK) status code' do
      expect(last_response.status).to be 200
    end
    it 'Returns the correct body' do
      expect(last_response.body).to include_json(key: 'value')
    end
  end
  describe :api_created do
    before do
      get '/created'
    end
    it 'Returns a 201 (Created) status code' do
      expect(last_response.status).to be 201
    end
    it 'Returns the correct body' do
      expect(last_response.body).to include_json(key: 'value')
    end
  end
  describe :api_empty do
    before do
      get '/empty'
    end
    it 'Returns a 204 (No Content) status code' do
      expect(last_response.status).to be 204
    end
    it 'Returns the correct body' do
      expect(last_response.body).to eq ''
    end
  end
end