FactoryBot.define do
  factory :session, class: Arkaan::Authentication::Session do
    session_id { 'session_token' }
  end
end