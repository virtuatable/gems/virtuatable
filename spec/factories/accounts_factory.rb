FactoryBot.define do
  factory :empty_account, class: Arkaan::Account do
    factory :babausse do
      username { 'Babausse' }
      email { 'courtois.vincent@outlook.com' }
      firstname { 'Vincent' }
      lastname { 'Courtois' }
      password { 'password' }
      password_confirmation { 'password' }
    end

    factory :account do
      username { Faker::Alphanumeric.alphanumeric(number: 16, min_alpha: 16) }
      email { Faker::Internet.unique.safe_email }
      password { 'password' }
      password_confirmation { 'password' }
    end
  end
end