FactoryBot.define do
  factory :empty_application, class: Arkaan::OAuth::Application do
    factory :application do
      name { 'application' }
      app_key { 'application_key' }
      premium { false }

      factory :premium_app do
        name { 'Premium application' }
        app_key { 'premium_key' }
        premium { true }
      end
    end
  end
end